# frozen_string_literal: true

require 'contentful'
require 'dotenv/load'

module Contentful
  def self.client
    Contentful::Client.new(
      space: ENV['SPACE_KEY'],
      access_token: ENV['ACCESS_TOKEN']
    )
  end

  def self.fetch_entries(entry)
    client.entries(content_type: entry)
  end

  def self.fetch_entry(entry_id)
    client.entry(entry_id)
  end
end
