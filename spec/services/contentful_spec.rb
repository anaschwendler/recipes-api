# frozen_string_literal: true

require_relative '../../services/contentful'
require_relative '../spec_helper'
require 'ostruct'

# rubocop:disable Metrics/BlockLength
RSpec.describe 'Contentful Test' do
  let(:recipes) { [{ 'title' => 'White Cheddar Grilled Cheese' }] }
  let(:recipe_struct) { OpenStruct.new(recipes.first) }

  describe '.fetch_entries' do
    before do
      allow(Contentful).to receive(:fetch_entries).with('recipe').and_return([recipe_struct])
    end

    context 'fetch recipes' do
      let(:fetch_recipes) { Contentful.fetch_entries('recipe') }

      it 'returns a list of recipes' do
        expect(fetch_recipes).to eq [recipe_struct]
      end

      it 'returns the correct title' do
        expect(fetch_recipes.first.title).to eq 'White Cheddar Grilled Cheese'
      end
    end
  end

  describe '.fetch_entry' do
    before do
      allow(Contentful).to receive(:fetch_entry).with('12').and_return(recipe_struct)
    end

    context 'fetch recipes' do
      let(:fetch_recipe) { Contentful.fetch_entry('12') }

      it 'returns a list of recipes' do
        expect(fetch_recipe).to eq recipe_struct
      end

      it 'returns the correct title' do
        expect(fetch_recipe.title).to eq 'White Cheddar Grilled Cheese'
      end
    end
  end
end
# rubocop:enable Metrics/BlockLength
