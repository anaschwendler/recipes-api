# frozen_string_literal: true

require_relative '../recipes'
require 'spec_helper'
require 'ostruct'

# rubocop:disable Metrics/BlockLength
RSpec.describe 'The Recipes App' do
  include Rack::Test::Methods
  let(:url) { { 'url' => '/' } }
  let(:recipes) do
    [
      {
        'title' => 'White Cheddar Grilled Cheese',
        'photo' => OpenStruct.new(url),
        'description' => 'Use delicious cheese and good quality bread'
      }
    ]
  end
  let(:recipe_struct) { OpenStruct.new(recipes.first) }

  def app
    Sinatra::Application
  end

  describe 'list recipes' do
    let(:response) { get '/' }

    before do
      allow(Contentful).to receive(:fetch_entries).with('recipe').and_return([recipe_struct])
    end

    context 'GET to /' do
      it 'returns status 200 OK' do
        expect(response.status).to eq 200
      end

      it 'displays a list of recipes' do
        expect(response.body).to include(
          "<p class='card-text'>White Cheddar Grilled Cheese</p>"
        )
      end
    end
  end

  describe 'show detailed recipe' do
    let(:response) { get '/recipes/12' }

    before do
      allow(Contentful).to receive(:fetch_entry).with('12').and_return(recipe_struct)
    end

    context 'GET to /recipes/:id' do
      it 'returns status 200 OK' do
        expect(response.status).to eq 200
      end

      it 'displays a list of recipes' do
        expect(response.body).to include(
          '<h3 class="my-3">White Cheddar Grilled Cheese</h3>'
        )

        expect(response.body).to include(
          '<p>Use delicious cheese and good quality bread</p>'
        )
      end
    end
  end
end
# rubocop:enable Metrics/BlockLength
