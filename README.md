# Recipes API

Hello everyone from Marley Spoon, firstly thanks for taking the time to review my challenge :)

## How it was done

I started the challenge by exploring a bit more of contentful and how could I use their ruby gem to fetch the entries and explore a bit more on how things could work.

After that, I've built the first structure for the core application, which was making the controller (in this case the `recipes.rb` file), and take a look at how the views for this controller would be placed in this configuration.

Following the base structure, I've decided to build a `/services` folder to be the place for the contentful requests I would need to do to fulfill the challenge. For me, that would be the structure for any external request I would do in a project.

Alongside that, I made sure to test everything (on the `/specs` folder) I've added to the code to make sure that everything was working properly so I could add more features to my project, easily. Also, I've set up a code linter to make sure that the code I was writing was following development patterns.

On top of that, I've added a CI (Continuous Integration) to the project, to make sure that everything that went to my main branch was concise and coherent. I didn't add a CD (Continuous Deployment) because I decided not to build a structure to deploy for this challenge.

## What was used

The base framework I decided to use to develop the challenge was [sinatra](http://sinatrarb.com/) because for me it is the lightweight way of developing a quick web app.

For the tests, I've used [rspec](https://rspec.info/) because I'm used to working with this gem to write tests.

To do request for Contentful I've used their ruby gem [contentful.rb](https://github.com/contentful/contentful.rb) because I noticed that it was well maintained, so it would be fine to give a try.


## What I would do more

Since I wanted to keep the application in a simple way, I skipped some development processes, so I preferred to list some things I would add to enhance the app, as it follows:

- [ ] Add more tests/checks from when an invalid request is made. An invalid request could be: request to non-existing pages, or without valid recipe ids, and so on.

- [ ] I saw that there where ways to "bulk" gather recipes instead of doing one simple call to the Contentful API, so I would work on refining the throughput of calls to the API.

- [ ] I would cache some requests, so there is no need to call the Contentful API all the time.

## Running the project

1. Configure your `.env` variables (you can copy `.env.example` and fill up with the corresponding keys)

2. Install the gems required for the project:

```
bundle install
```

3. Run the application

```
make run
```

Then go to http://localhost:4567

### Running the tests

```
make test
```

### Running the linter

```
make lint
```


## Screenshots

### Listing Recipes

![image](https://user-images.githubusercontent.com/4131432/87915896-04df1500-ca73-11ea-84b6-97b97e2d9f8e.png)

### Detailed Recipe

![image](https://user-images.githubusercontent.com/4131432/87917981-0d851a80-ca76-11ea-97a5-9d0f9f16803f.png)
