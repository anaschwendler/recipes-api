dependencies:
	gem install bundler
	bundle install

lint:
	bundle exec rubocop

run:
	ruby recipes.rb

test:
	RAILS_ENV=test bundle exec rspec
